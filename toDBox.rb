#!/usr/bin/env

require 'English'
require 'optparse'
require 'dropbox_sdk'
require 'shellwords'

class Connection

	ACCESS_TOKEN = ''
	@client
	
	def initialize
		access_token = ACCESS_TOKEN
		@client = DropboxClient.new(access_token)
	end
	
	def upload_file(dest)
		#the first parameter of put_file is the name of the file you will see in your dropbox account,
		#the second parameter is the destination of the file you want to upload
		response = @client.put_file('backup.tar', open(dest))
		puts "File uploaded"
	end

end

class Application
	
	@argument
	
	def initialize
		@argument = {}
	end
	
	def process()
		
		parser = OptionParser.new do | option |
			app_name = File.basename($PROGRAM_NAME)
			option.banner = "Backup one or more files to your Dropbox account.
			Usage: #{app_name} [option]"
			option.on("-d PATH", "--directory PATH", "Indicates the directory to backup") do | directory |
				@argument[:directory] = directory
			end
		end

		parser.parse!
		if @argument.empty?
			puts "Error: Missing operands"
			puts parser.help
			exit 2 #exit 2 means the command is not being used properly
		else
			#if you introduce a line like this '/folder/file with a space.txt', tar command returns an error.
			#To avoid that, we need to use shellescape, that line will be proccessed like this /folder/file\ with\ a\ space.txt
			file_name_shellescape = @argument[:directory].shellescape
			backup_task = "tar -cPf backup.tar #{file_name_shellescape}"
			system(backup_task) #system is used to call system programs
			file_name = "backup.tar"

			unless $CHILD_STATUS.exitstatus == 0
				puts "You must use a valid path to create the backup file"
				clean() #if tar fails, it still creates a empty file
				exit 1
			else
				puts "Backup successfully created"
			end

			instance = Connection.new
			instance.upload_file(file_name)
			clean()
		end
	end
	
	def clean()
		remove_task = "rm -f backup.tar"
		system(remove_task)
	end	

end

if __FILE__ == $0
	app = Application.new
	app.process
end